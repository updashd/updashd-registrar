<?php
namespace Updashd;

use Predis\Client;
use Updashd\Doctrine\Manager;
use Updashd\Model\MetricType;
use Updashd\Model\Service;
use Updashd\Model\ServiceMetricField;
use Updashd\Scheduler\Scheduler;

class Registrar {
    private $config;
    private $client;
    private $manager;
    
    public function __construct ($config) {
        // Store the configuration
        $this->setConfig($config);
    
        // Create the Predis Client
        $client = new Client($this->getConfig('predis'));
        $this->setClient($client);

        $manager = new Manager($config['doctrine']);
        $this->setManager($manager);
    }

    /**
     * Run the worker
     */
    public function run () {
        $scheduler = new Scheduler($this->getClient(), $this->getConfig('zone'));

        while ($serviceSpec = $scheduler->getServiceRegistrationBlocking()) {
            $moduleName = $serviceSpec->getModuleName();
            $readableName = $serviceSpec->getReadableName();

            $em = $this->getEntityManager();

            // Try to retrieve existing service
            $serviceRepo = $em->getRepository(Service::class);
            $service = $serviceRepo->findOneBy(['moduleName' => $moduleName]);

            if (! $service) {
                printf("Registering Service: %s (%s)\n", $moduleName, $readableName);

                $service = new Service();
                $service->setModuleName($moduleName);
                $service->setSortOrder($this->getNextSortOrder());
            }

            $service->setServiceName($readableName);

            $em->persist($service);
            $em->flush();

            $serviceMetricRepo = $em->getRepository(ServiceMetricField::class);

            foreach ($serviceSpec->getFields() as $field) {
                $fieldName = $field->getKey();

                $serviceMetric = $serviceMetricRepo->findOneBy([
                    'service' => $service,
                    'fieldName' => $fieldName
                ]);

                $name = $field->getName();
                $unit = $field->getUnit();

                if (! $serviceMetric) {
                    $type = $field->getType();

                    printf("Registering Service Field: %s (%s, type: %s, unit: %s)\n", $fieldName, $name, $type, $unit);

                    $serviceMetric = new ServiceMetricField();
                    $serviceMetric->setService($service);
                    $serviceMetric->setFieldName($fieldName);
                    $serviceMetric->setMetricType($em->getReference(MetricType::class, $type));
                }

                // We can update the readable name and unit. Everything else MUST remain as is.
                $serviceMetric->setReadableName($name);
                $serviceMetric->setUnit($unit);

                $em->persist($serviceMetric);
            }

            $em->flush();

            $em->clear(); // Attempting to resolve memory leaks
        }
    }

    /**
     * Retrieve the config, or a key from it.
     * @param string $key
     * @return array|mixed
     */
    public function getConfig ($key = null) {
        if ($key && array_key_exists($key, $this->config)) {
            return $this->config[$key];
        }
        
        return $this->config;
    }
    
    /**
     * @param array $config
     */
    public function setConfig ($config) {
        $this->config = $config;
    }
    
    /**
     * @return Client
     */
    public function getClient () {
        return $this->client;
    }
    
    /**
     * @param Client $client
     */
    public function setClient (Client $client) {
        $this->client = $client;
    }
    
    /**
     * @return Manager
     */
    public function getManager () {
        return $this->manager;
    }

    /**
     * @param Manager $manager
     */
    public function setManager (Manager $manager) {
        $this->manager = $manager;
    }

    protected function getNextSortOrder () {
        $em = $this->getEntityManager();

        $serviceRepo = $em->getRepository(Service::class);

        $allServices = $serviceRepo->findAll();

        $maxSortOrder = 0;

        /** @var Service $service */
        foreach ($allServices as $service) {
            $sortOrder = $service->getSortOrder();

            if ($sortOrder > $maxSortOrder) {
                $maxSortOrder = $sortOrder;
            }
        }

        return $maxSortOrder + 1;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager () {
        return $this->getManager()->getEntityManager();
    }
}